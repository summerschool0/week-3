#include <iostream>

void vectorIterators()
{
	// Create a vector of ints and push elements 1->5 inside
	// Iterate the vector using begin - end
	// What about constant iteration
	// Can we iterate reverse?
	// Constant reverse
}

void vectorCapacity()
{
	// Create a vector of ints and push elements 1->5 inside

	std::cout << "Size : ";
	std::cout << "\nMax_Size : ";

	// resizes the vector size to 4

	// prints the vector size after resize()
	std::cout << "\nSize : ";

	// checks if the vector is empty or not

	std::cout << "\nVector elements are: ";
}

void vectorModifiers()
{
	// Assign vector
	// fill the array with 10 five times

	std::cout << "The vector elements are: ";

	// inserts 15 to the last position

	std::cout << "\nThe last element is: ";

	// removes last element

	// prints the vector
	std::cout << "\nThe vector elements are: ";

	// inserts 5 at the beginning

	std::cout << "\nThe first element is: ";

	// removes the first element

	std::cout << "\nThe first element is: ";

	// Inserts 20 at the end
	std::cout << "\nThe last element is: ";

	// erases the vector


	std::cout << "\nVector size after erase(): ";
}

void vectorAccess()
{
	// Create a vector of ints and push some elements 

	std::cout << "\nReference operator [] = ";

	std::cout << "\nat = ";

	std::cout << "\nGet the first element = ";

	std::cout << "\nGet the last element = ";

	// pointer to the first element


	std::cout << "\nThe first element is ";
}


void howToList()
{
	// Create and fill a list of ints

	std::cout << "\nList is : ";

	std::cout << "\nGet the first element = ";

	std::cout << "\nGet the last element = ";

	// Remove the first element

	// Remove the last element

	// Use emplace to add an element

	// Sort the list

	std::cout << "\nList is : ";
}


void howToDeque()
{
	// Create a deque of ints

	// Push front and back some given elements

	std::cout << "The deque gquiz is : ";


	// Remove the first elemt

	// Remove the last element


	std::cout << "The deque gquiz is : ";


	// Add a new element "in place" in the deque
}

void howToArray()
{
	// Initializing the array elements 


	// Operations on array :-
	// 1. at() : -This function is used to access the elements of array.
	// 2. get() : -This function is also used to access the elements of array.This function is not the member of array class but overloaded function from class tuple.
	// 3. operator[] :-This is similar to C - style arrays.This method is also used to access array elements.
	// 4. front() :-This returns the first element of array.
	// 5. back() : -This returns the last element of array.
	// 6. size() : -It returns the number of elements in array.This is a property that C - style arrays lack.
	// 7. max_size() : -It returns the maximum number of elements array can hold i.e, the size with which array is declared.The size() and max_size() return the same value.
	// 8. swap() : -The swap() swaps all elements of one array with other.
	// 9. empty() : -This function returns true when the array size is zero else returns false.
	// 10. fill() :-This function is used to fill the entire array with a particular value.
}

void howToQueue()
{
	// Create a queue and push some elements

	std::cout << "The queue gquiz is : ";

	std::cout << "\nSize : ";
	std::cout << "\nFirst element: ";
	std::cout << "\nLast element : ";

	std::cout << "\nPop : ";

	std::cout << "The queue gquiz is : ";
}


void howToStack()
{
	// Create a queue and push some elements

	std::cout << "The stack gquiz is : ";

	std::cout << "\nSize: ";
	std::cout << "\nTop : ";

	std::cout << "\nPop : ";

	std::cout << "The stack gquiz is : ";
}

void howToSet()
{
	// empty int set container with greater as compare key

	// insert elements in random order

	// insert the same element twice

	// print the set

	// remove all elements up to a chosen number

	// print

	// lower bound (greater or equal )


	// upper bound (greater than value)

}

void howToMap()
{
	// empty map container

	// insert element in map in all methods

	// print

	// remove all elements up to element with key= ?  in myMap

	// print
}

void SequenceContainers()
{
	////////////////////////////
	/// Vector
	////////////////////////////
	//
	// Vectors are same as dynamic arrays with the ability to resize itself automatically when an element is inserted or deleted,
	// with their storage being handled automatically by the container. Vector elements are placed in contiguous storage so that
	// they can be accessed and traversed using iterators. In vectors, data is inserted at the end. Inserting at the end takes differential
	// time, as sometimes there may be a need of extending the array. Removing the last element takes only constant time because no resizing
	// happens. Inserting and erasing at the beginning or in the middle is linear in time.

	vectorIterators();
	vectorCapacity();
	vectorModifiers();
	vectorAccess();

	////////////////////////////
	/// List
	////////////////////////////
	//
	// Lists are sequence containers that allow non-contiguous memory allocation. As compared to vector, list has slow traversal,
	// but once a position has been found, insertion and deletion are quick. Normally, when we say a List, we talk about doubly linked list.
	// For implementing a singly linked list, we use forward list.

	howToList();

	////////////////////////////
	/// Deque
	////////////////////////////
	//
	// Double ended queues are sequence containers with the feature of expansion and contraction on both the ends. 
	// They are similar to vectors, but are more efficient in case of insertion and deletion of elements at the end, and also the beginning. 
	// Unlike vectors, contiguous storage allocation may not be guaranteed.
	// The functions for deque are same as vector, with an addition of push and pop operations for both front and backto

	howToDeque();

	////////////////////////////
	/// Array
	////////////////////////////
	//
	// The introduction of array class from C++11 has offered a better alternative for C - style arrays.
	// The advantages of array class over C - style array are :
	// - Array classes knows its own size, whereas C - style arrays lack this property.So when passing to functions, we dont need to pass size of Array as a separate parameter.
	// - With C - style array there is more risk of array being decayed into a pointer.Array classes dont decay into pointers
	// - Array classes are generally more efficient, light - weight and reliable than C - style arrays.

	howToArray();

	////////////////////////////
	/// Forward List
	////////////////////////////
	//
	// Forward list in STL implements singly linked list.Introduced from C++11, forward list are useful than other containers in insertion, 
	// removal and moving operations(like sort) and allows time constant insertion and removal of elements.
	// 
	// It differs from list by the fact that forward list keeps track of location of only next element while list keeps track to both next 
	// and previous elements, thus increasing the storage space required to store each element.The drawback of forward list is that it cannot be 
	// iterated backwards and its individual elements cannot be accessed directly.
	//
	// Forward List is preferred over list when only forward traversal is required(same as singly linked list is preferred over doubly linked list)
	// as we can save space.Some example cases are, chaining in hashing, adjacency list representation of graph, etc.
}

void ContainerAdaptors()
{
	////////////////////////////
	/// Queue 
	////////////////////////////
	//
	// Queues are a type of container adaptors which operate in a first in first out(FIFO) type of arrangement.
	// Elements are inserted at the back(end) and are deleted from the front.
	// The functions supported by queue are :
	//
	//  empty()  Returns whether the queue is empty
	//  size()  Returns the size of the queue
	//  front()  Returns a reference to the first element of the queue
	//  back()  Returns a reference to the last element of the queue
	//  push(g)  Adds the element g at the end of the queue
	//  pop()  Deletes the first element of the queue

	howToQueue();

	////////////////////////////
	/// Stack 
	////////////////////////////
	//
	// Stacks are a type of container adaptors with LIFO(Last In First Out) type of working, where a new element is added 
	// at one end and (top)an element is removed from that end only.
	//
	// The functions associated with stack are :
	//      empty()  Returns whether the stack is empty
	//      size()  Returns the size of the stack
	//      top()  Returns a reference to the top most element of the stack
	//      push(g)  Adds the element g at the top of the stack
	//      pop()  Deletes the top most element of the stack

	howToStack();
}

void AssociativeContainers()
{
	////////////////////////////
	/// Set 
	////////////////////////////
	//
	// Sets are a type of associative containers in which each element has to be unique, because the value of the element identifies it.
	// The value of the element cannot be modified once it is added to the set, though it is possible to remove and add the modified value of that element.

	howToSet();

	////////////////////////////
	/// Map 
	////////////////////////////
	//
	// Maps are associative containers that store elements in a mapped fashion.Each element has a key value and a mapped value.
	// No two mapped values can have same key values.

	howToMap();
}

void Containers()
{
	SequenceContainers();
	ContainerAdaptors();
	AssociativeContainers();
}

int increment(int x) { return (x + 1); }

void FunctorAndTransform()
{
	// init a vector of ints
	// Apply increment to all elements of arr[] and store the modified elements back in arr[] using a functor function
}

void Lambda()
{
	// Lambda syntax

	// Lambda variable

	// Lambda capture

	// Lambda argument
}

void LambdaAndAlgo()
{
	// Init a vector and push some elements

	// use for_each to multiply each element by 10

	// use for_each to print all elements


	// Init a vector

	// Resize it

	// Using fill to "fill" the vector

	// use for_each to print all elements

	//std::for_each_n(ns.begin(), 3, [](auto& n) { n *= 2; });
}

void Functors()
{
	FunctorAndTransform();

	Lambda();

	LambdaAndAlgo();
}

void Algo()
{
	//sort(first_iterator, last_iterator)  To sort the given vector.
	//reverse(first_iterator, last_iterator)  To reverse a vector.
	//max_element(first_iterator, last_iterator)  To find the maximum element of a vector.
	//min_element(first_iterator, last_iterator)  To find the minimum element of a vector.
	//accumulate(first_iterator, last_iterator, initial value of sum)  Does the summation of vector elements
	//count(first_iterator, last_iterator, x)  To count the occurrences of x in vector.
	//find(first_iterator, last_iterator, x)  Points to last address of vector((name_of_vector).end()) if element is not present in vector.
	//lower_bound(first_iterator, last_iterator, x)  returns an iterator pointing to the first element in the range[first, last) which has a value not less than x.
	//upper_bound(first_iterator, last_iterator, x)  returns an iterator pointing to the first element in the range[first, last) which has a value greater than x.
	//next_permutation(first_iterator, last_iterator)  This modified the vector to its next permutation.
	//prev_permutation(first_iterator, last_iterator)  This modified the vector to its previous permutation.
	//distance(first_iterator,desired_position)
}

int main()
{
	Containers();

	//Functors();

	//Algo();


	std::cin.get();
	// Create a vector of ints and push elements 1->5 inside
// Iterate the vector using begin - end
// What about constant iteration
// Can we iterate reverse?
// Constant reverse
}

void vectorCapacity()
{
	// Create a vector of ints and push elements 1->5 inside

	std::cout << "Size : ";
	std::cout << "\nMax_Size : ";

	// resizes the vector size to 4

	// prints the vector size after resize()
	std::cout << "\nSize : ";

	// checks if the vector is empty or not

	std::cout << "\nVector elements are: ";
}

void vectorModifiers()
{
	// Assign vector
	// fill the array with 10 five times

	std::cout << "The vector elements are: ";

	// inserts 15 to the last position

	std::cout << "\nThe last element is: ";

	// removes last element

	// prints the vector
	std::cout << "\nThe vector elements are: ";

	// inserts 5 at the beginning

	std::cout << "\nThe first element is: ";

	// removes the first element

	std::cout << "\nThe first element is: ";

	// Inserts 20 at the end
	std::cout << "\nThe last element is: ";

	// erases the vector


	std::cout << "\nVector size after erase(): ";
}

void vectorAccess()
{
	// Create a vector of ints and push some elements 

	std::cout << "\nReference operator [] = ";

	std::cout << "\nat = ";

	std::cout << "\nGet the first element = ";

	std::cout << "\nGet the last element = ";

	// pointer to the first element


	std::cout << "\nThe first element is ";
}


void howToList()
{
	// Create and fill a list of ints

	std::cout << "\nList is : ";

	std::cout << "\nGet the first element = ";

	std::cout << "\nGet the last element = ";

	// Remove the first element

	// Remove the last element

	// Use emplace to add an element

	// Sort the list

	std::cout << "\nList is : ";
}


void howToDeque()
{
	// Create a deque of ints

	// Push front and back some given elements

	std::cout << "The deque gquiz is : ";


	// Remove the first elemt

	// Remove the last element


	std::cout << "The deque gquiz is : ";


	// Add a new element "in place" in the deque
}

void howToArray()
{
	// Initializing the array elements 


	// Operations on array :-
	// 1. at() : -This function is used to access the elements of array.
	// 2. get() : -This function is also used to access the elements of array.This function is not the member of array class but overloaded function from class tuple.
	// 3. operator[] :-This is similar to C - style arrays.This method is also used to access array elements.
	// 4. front() :-This returns the first element of array.
	// 5. back() : -This returns the last element of array.
	// 6. size() : -It returns the number of elements in array.This is a property that C - style arrays lack.
	// 7. max_size() : -It returns the maximum number of elements array can hold i.e, the size with which array is declared.The size() and max_size() return the same value.
	// 8. swap() : -The swap() swaps all elements of one array with other.
	// 9. empty() : -This function returns true when the array size is zero else returns false.
	// 10. fill() :-This function is used to fill the entire array with a particular value.
}

void howToQueue()
{
	// Create a queue and push some elements

	std::cout << "The queue gquiz is : ";

	std::cout << "\nSize : ";
	std::cout << "\nFirst element: ";
	std::cout << "\nLast element : ";

	std::cout << "\nPop : ";

	std::cout << "The queue gquiz is : ";
}


void howToStack()
{
	// Create a queue and push some elements

	std::cout << "The stack gquiz is : ";

	std::cout << "\nSize: ";
	std::cout << "\nTop : ";

	std::cout << "\nPop : ";

	std::cout << "The stack gquiz is : ";
}

void howToSet()
{
	// empty int set container with greater as compare key

	// insert elements in random order

	// insert the same element twice

	// print the set

	// remove all elements up to a chosen number

	// print

	// lower bound (greater or equal )


	// upper bound (greater than value)

}

void howToMap()
{
	// empty map container

	// insert element in map in all methods

	// print

	// remove all elements up to element with key= ?  in myMap

	// print
}

void SequenceContainers()
{
	////////////////////////////
	/// Vector
	////////////////////////////
	//
	// Vectors are same as dynamic arrays with the ability to resize itself automatically when an element is inserted or deleted,
	// with their storage being handled automatically by the container. Vector elements are placed in contiguous storage so that
	// they can be accessed and traversed using iterators. In vectors, data is inserted at the end. Inserting at the end takes differential
	// time, as sometimes there may be a need of extending the array. Removing the last element takes only constant time because no resizing
	// happens. Inserting and erasing at the beginning or in the middle is linear in time.

	vectorIterators();
	vectorCapacity();
	vectorModifiers();
	vectorAccess();

	////////////////////////////
	/// List
	////////////////////////////
	//
	// Lists are sequence containers that allow non-contiguous memory allocation. As compared to vector, list has slow traversal,
	// but once a position has been found, insertion and deletion are quick. Normally, when we say a List, we talk about doubly linked list.
	// For implementing a singly linked list, we use forward list.

	howToList();

	////////////////////////////
	/// Deque
	////////////////////////////
	//
	// Double ended queues are sequence containers with the feature of expansion and contraction on both the ends. 
	// They are similar to vectors, but are more efficient in case of insertion and deletion of elements at the end, and also the beginning. 
	// Unlike vectors, contiguous storage allocation may not be guaranteed.
	// The functions for deque are same as vector, with an addition of push and pop operations for both front and backto

	howToDeque();

	////////////////////////////
	/// Array
	////////////////////////////
	//
	// The introduction of array class from C++11 has offered a better alternative for C - style arrays.
	// The advantages of array class over C - style array are :
	// - Array classes knows its own size, whereas C - style arrays lack this property.So when passing to functions, we dont need to pass size of Array as a separate parameter.
	// - With C - style array there is more risk of array being decayed into a pointer.Array classes dont decay into pointers
	// - Array classes are generally more efficient, light - weight and reliable than C - style arrays.

	howToArray();

	////////////////////////////
	/// Forward List
	////////////////////////////
	//
	// Forward list in STL implements singly linked list.Introduced from C++11, forward list are useful than other containers in insertion, 
	// removal and moving operations(like sort) and allows time constant insertion and removal of elements.
	// 
	// It differs from list by the fact that forward list keeps track of location of only next element while list keeps track to both next 
	// and previous elements, thus increasing the storage space required to store each element.The drawback of forward list is that it cannot be 
	// iterated backwards and its individual elements cannot be accessed directly.
	//
	// Forward List is preferred over list when only forward traversal is required(same as singly linked list is preferred over doubly linked list)
	// as we can save space.Some example cases are, chaining in hashing, adjacency list representation of graph, etc.
}

void ContainerAdaptors()
{
	////////////////////////////
	/// Queue 
	////////////////////////////
	//
	// Queues are a type of container adaptors which operate in a first in first out(FIFO) type of arrangement.
	// Elements are inserted at the back(end) and are deleted from the front.
	// The functions supported by queue are :
	//
	//  empty()  Returns whether the queue is empty
	//  size()  Returns the size of the queue
	//  front()  Returns a reference to the first element of the queue
	//  back()  Returns a reference to the last element of the queue
	//  push(g)  Adds the element g at the end of the queue
	//  pop()  Deletes the first element of the queue

	howToQueue();

	////////////////////////////
	/// Stack 
	////////////////////////////
	//
	// Stacks are a type of container adaptors with LIFO(Last In First Out) type of working, where a new element is added 
	// at one end and (top)an element is removed from that end only.
	//
	// The functions associated with stack are :
	//      empty()  Returns whether the stack is empty
	//      size()  Returns the size of the stack
	//      top()  Returns a reference to the top most element of the stack
	//      push(g)  Adds the element g at the top of the stack
	//      pop()  Deletes the top most element of the stack

	howToStack();
}

void AssociativeContainers()
{
	////////////////////////////
	/// Set 
	////////////////////////////
	//
	// Sets are a type of associative containers in which each element has to be unique, because the value of the element identifies it.
	// The value of the element cannot be modified once it is added to the set, though it is possible to remove and add the modified value of that element.

	howToSet();

	////////////////////////////
	/// Map 
	////////////////////////////
	//
	// Maps are associative containers that store elements in a mapped fashion.Each element has a key value and a mapped value.
	// No two mapped values can have same key values.

	howToMap();
}

void Containers()
{
	SequenceContainers();
	ContainerAdaptors();
	AssociativeContainers();
}

int increment(int x) { return (x + 1); }

void FunctorAndTransform()
{
	// init a vector of ints
	// Apply increment to all elements of arr[] and store the modified elements back in arr[] using a functor function
}

void Lambda()
{
	// Lambda syntax

	// Lambda variable

	// Lambda capture

	// Lambda argument
}

void LambdaAndAlgo()
{
	// Init a vector and push some elements

	// use for_each to multiply each element by 10

	// use for_each to print all elements


	// Init a vector

	// Resize it

	// Using fill to "fill" the vector

	// use for_each to print all elements

	//std::for_each_n(ns.begin(), 3, [](auto& n) { n *= 2; });
}

void Functors()
{
	FunctorAndTransform();

	Lambda();

	LambdaAndAlgo();
}

void Algo()
{
	//sort(first_iterator, last_iterator)  To sort the given vector.
	//reverse(first_iterator, last_iterator)  To reverse a vector.
	//max_element(first_iterator, last_iterator)  To find the maximum element of a vector.
	//min_element(first_iterator, last_iterator)  To find the minimum element of a vector.
	//accumulate(first_iterator, last_iterator, initial value of sum)  Does the summation of vector elements
	//count(first_iterator, last_iterator, x)  To count the occurrences of x in vector.
	//find(first_iterator, last_iterator, x)  Points to last address of vector((name_of_vector).end()) if element is not present in vector.
	//lower_bound(first_iterator, last_iterator, x)  returns an iterator pointing to the first element in the range[first, last) which has a value not less than x.
	//upper_bound(first_iterator, last_iterator, x)  returns an iterator pointing to the first element in the range[first, last) which has a value greater than x.
	//next_permutation(first_iterator, last_iterator)  This modified the vector to its next permutation.
	//prev_permutation(first_iterator, last_iterator)  This modified the vector to its previous permutation.
	//distance(first_iterator,desired_position)
}

int main()
{
	Containers();

	//Functors();

	//Algo();


	std::cin.get();
	return 0;
}