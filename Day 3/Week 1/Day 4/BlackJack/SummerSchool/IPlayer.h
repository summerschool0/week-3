#pragma once

class Hand;
enum Action;

class IPlayer
{
	virtual const char* GetName() const = 0;
	virtual Hand& GetHand() = 0;
	virtual Action GetAction() const = 0;
};

