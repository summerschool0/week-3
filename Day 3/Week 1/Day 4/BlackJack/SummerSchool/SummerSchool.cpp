// SummerSchool.cpp : Defines the entry point for the console application.
//

#include "GDeck.h"
#include "GCard.h"

#include "Card.h";
#include "Player.h"
#include "Game.h"
#include "NaturalPolicy.h"
#include "WinnerTakesAll.h"
#include "Config.h"
#include "Application.h"
#include <iostream>

#include <SFML/Graphics.hpp>

int main()
{
	Card* card = new Card(Suit::CLUBS, '2');

	GCard gCard(card);
	Deck deck;
	GDeck gdeck;
	gdeck.LoadFromDeck(deck);
	
	sf::RenderWindow window(sf::VideoMode(200, 200), "SFML works!");
	sf::CircleShape shape(100.f);
	shape.setFillColor(sf::Color::Green);

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}

		window.clear();
		window.draw(gCard.sprite);
		window.display();
	}

	/* Run it */
	//application();
	return 0;
}


//int main()
//{
//	Player* players[] =
//	{
//		new Player("Player 1"),
//		new Player("Player 2")
//	};
//	size_t numPlayers = sizeof(players) / sizeof(Player*);
//
//	NaturalPolicy* naturalPolicy = new NaturalPolicy();
//	WinnerTakesAll* winnerTakesAll = new WinnerTakesAll();
//
//	int minNo;
//	minNo = Config::getInstance()->GetSetting(Config::Settings::minPlayers);
//
//	Game game(numPlayers, players, naturalPolicy);
//
//	game.Play();
//
//	for (Player* player : players)
//	{
//		delete player;
//		player = nullptr;
//	}
//	
//    return 0;
//}

