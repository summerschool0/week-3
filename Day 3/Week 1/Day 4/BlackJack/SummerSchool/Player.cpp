
#include "Player.h"
#include <cstring>

Player::Player(const char * name, double totalMoney) :
	m_name(nullptr), 
	m_hand(),
	m_totalMoney(totalMoney)
{
	m_name = new char[strlen(name) + 1]; // add 1 for '\0'
	strcpy(m_name, name);
}

Player::~Player()
{
	delete[] m_name;
	m_name = nullptr;
}

const char * Player::GetName() const
{
	return m_name;
}

Hand & Player::GetHand()
{
	return m_hand;
}

Action Player::GetAction() const
{
	Action returnValue = STAND;
	if (m_hand.GetValue() < 17) returnValue = HIT;

	return returnValue;
}

double Player::getTotalMoney() const
{
	return m_totalMoney;
}

void Player::setTotalMoney(double totalMoney)
{
	m_totalMoney = totalMoney;
}
