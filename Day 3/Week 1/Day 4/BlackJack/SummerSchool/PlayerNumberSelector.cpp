#include "PlayerNumberSelector.h"
#include "Application.h"
#include "MainMenu.h"

PlayerNumberSelector::PlayerNumberSelector()
{
	/*TwoPlayersButton;
	ThreePlayersButton = Button(sf::Vector2f(10, 20), sf::Vector2f(200, 100));
	FourPlayersButton = Button(sf::Vector2f(10, 20), sf::Vector2f(200, 100));*/
	TwoPlayersButton.setSize(sf::Vector2f(100, 30));
	TwoPlayersButton.setPosition(sf::Vector2f(50, 20));
	TwoPlayersButton.setColour(sf::Color::Blue);
	TwoPlayersButton.setText("Two Players");

	ThreePlayersButton.setSize(sf::Vector2f(100, 30));
	ThreePlayersButton.setPosition(sf::Vector2f(50, 50));
	ThreePlayersButton.setColour(sf::Color::Red);
	ThreePlayersButton.setText("Three Players");

	FourPlayersButton.setSize(sf::Vector2f(100, 30));
	FourPlayersButton.setPosition(sf::Vector2f(50, 80));
	FourPlayersButton.setColour(sf::Color::Green);
	FourPlayersButton.setText("Four Players");
}

void PlayerNumberSelector::update(sf::RenderWindow& window)
{
	handleEvent(window);
}

void PlayerNumberSelector::draw(sf::RenderWindow& window)
{
	window.draw(TwoPlayersButton);
	window.draw(ThreePlayersButton);
	window.draw(FourPlayersButton);
}

void PlayerNumberSelector::handleEvent(sf::RenderWindow& window)
{
	sf::Event event;
	while (window.pollEvent(event))
	{
		/* If any key was pressed, go back to the main menu */
		if (event.type == sf::Event::MouseButtonPressed || event.type == sf::Event::KeyPressed)
		{
			if (TwoPlayersButton.isCollisonWithPoint(sf::Vector2f(sf::Mouse::getPosition(window))))
			{
				Application::getInstance()->stateController = std::make_unique<MainMenu>();
			}
		}
	}
}
