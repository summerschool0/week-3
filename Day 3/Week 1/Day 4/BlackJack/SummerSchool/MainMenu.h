#pragma once
#include "Game.h"
#include "Application.h"
#include "Button.h"

class MainMenu : public IStateClass
{
public:
	MainMenu();
	~MainMenu();

private:
//	Button startButton, multiplayerButton, levelEditorButton, exitButton;
//	Button gameName;

public:

	void update(sf::RenderWindow& window);
	void draw(sf::RenderWindow& window);
	void handleEvent(sf::RenderWindow& window);

};
