#pragma once
#include "SFML/Graphics.hpp"

class Button : public sf::Drawable
{
public:
	Button();
	Button(const sf::Vector2f& position, const sf::Vector2f& size);
	~Button() = default;

private:
	sf::Font m_font;
	sf::Text m_text;

	sf::RectangleShape m_hitbox;

private:
	void setUpButton();

public:
	sf::Font getFont() const;
	void setFont(const std::string& font);
		
	sf::Text getText() const;
	void setText(const std::string& text);

	sf::Color getColour() const;
	void setColour(const sf::Color& color);

	sf::Vector2f getPosition() const;
	void setPosition(const sf::Vector2f& position);

	sf::Vector2f getSize() const;
	void setSize(const sf::Vector2f& size);

	bool isCollisonWithPoint(const sf::Vector2f& point) const;
	bool isCollisionWithRect(const sf::RectangleShape& hitBox) const;

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
};

