#pragma once
#include <SFML\Graphics.hpp>
#include <fstream>
#include "IStateClass.h"

#include "Game.h"

class Application
{
public:
	Application(const std::string& title, const int32_t x, const int32_t y);
	~Application();

	/* The operator starts the game loop basically */
	void operator () ();

private:

	static Application* m_instance;
	sf::RenderWindow m_window;
	sf::Event m_event;

	
	/* Time to render a frame - Unties the game from the graphics card */
	const sf::Time frameTime = sf::seconds(1.0f / 60.f);
	const sf::Color backgroundColor = sf::Color(241, 241, 241);

public:
	std::unique_ptr<IStateClass> stateController;
	/* Event handling */
	void handleEvent();
	void gameLoop();

public:

	/* Getters */
	static Application* getInstance();
	sf::Vector2u getSize();

	/* Setters */
	void setTitle(const std::string& newTitle);
	void setSize(const int32_t x, const int32_t y);
};

