#pragma once

namespace ErrorCode
{
	static const char* FileNotFound = "File was not found!";
	static constexpr char* KeyNotFound = "Key was not found!";
	static constexpr char* ValueNotFound = "Value was not found!";
	static constexpr char* MultipleEqualsFound = "Multiple == signs were found, please check the configuration file!";

	class FileNotFoundException : std::exception
	{
	public:
		const char* what()
		{
			return FileNotFound;
		}
	};


};

