#pragma once
#include "Deck.h"
#include "Shoe.h"
#include "IPlayer.h"
#include <unordered_map>

class Player;
class Dealer
{
public:
	Dealer();
	~Dealer();

	void Shuffle();
	void Deal(Player* player);

	void AddPlayer(Player* player);
	std::unordered_map<Player*, double>& getBettedMoney();

	bool Bet(Player* player, double amount);

private:

	Deck m_deck;
	Shoe m_shoe;

	std::unordered_map<Player*, double> m_bettedMoney;
};

