#pragma once

#include "GDeck.h"
#include "GCard.h"

GDeck::GDeck()
{
	// EMPTY
}

void GDeck::LoadFromDeck(const Deck& deck)
{
	auto cards = deck.GetCards();
	
	for (int i = 0; i < deck.GetNumberOfCards(); ++i)
	{
		m_gCards.emplace_back(cards[i] );
	}
}


