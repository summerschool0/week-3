#pragma once
#include <SFML\Graphics.hpp>
#include <memory>
#include "Card.h"

class GCard
{
public:
	GCard();
	GCard(Card* card);
	
public:
	sf::Sprite sprite;

private:
	Card* card;
};

