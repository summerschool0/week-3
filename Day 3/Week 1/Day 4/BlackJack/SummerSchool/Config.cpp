#include "Config.h"
#include <algorithm>
#include <utility>
#include <iostream>


Config* Config::m_instance = nullptr;
Config::Config(const std::string& filePath)
{
	if (m_instance == nullptr)
		m_instance = this;

	initializeStrings();
	load(filePath);
}

void Config::readConfigs()
{
	if (!m_file)
		throw ErrorCode::FileNotFoundException();
	
	static const char separator = '=';
	std::string key;
	std::string value;

	while (std::getline(m_file, key, separator) && std::getline(m_file, value, separator))
	{
		/* Remove whitespace */
		key.erase(remove_if(key.begin(), key.end(), isspace), key.end());
		value.erase(remove_if(value.begin(), value.end(), isspace), value.end());

		/* Checks */
		if (key == "" || value == "")
			continue; // We ignore empty results

		Config::Settings settingValue = getSettingFromString(key);
		if (settingValue == Config::Settings::NONE)
			continue;

		auto parsedValue = std::stoi(value);
		if (parsedValue == 0)
			throw ErrorCode::ValueNotFound;

		m_settings.insert(std::make_pair(settingValue, parsedValue));
	}
}

Config::Settings Config::getSettingFromString(std::string& input)
{
	if (input == "")
		return Config::Settings::NONE;

	auto it = m_strings.find(input);
	if (it == m_strings.end())
		return Config::Settings::NONE;

	return m_strings.at(input);
}

void Config::initializeStrings()
{
	m_strings.insert({ "none", Config::Settings::NONE });
	m_strings.insert({ "bot_hits_below", Settings::botHitsBelow });
	m_strings.insert(std::make_pair("min_players", Settings::minPlayers));
	m_strings.insert(std::make_pair("max_players", Settings::maxPlayers));
}

bool Config::Save() const
{
	return false;
}

void Config::load(const std::string& filePath)
{
	m_file.open(filePath);
	if (m_file.fail())
		throw ErrorCode::FileNotFoundException();

	return readConfigs();
}


int Config::GetSetting(const Settings& settingType)
{
	auto it = m_settings.find(settingType);

	if (it == m_settings.end())
		throw ErrorCode::KeyNotFound;

	return m_settings[settingType]; // Success
}

Config* Config::getInstance(const std::string& filePath)
{
	if (m_instance == nullptr)
	{
		try
		{
			m_instance = new Config(filePath);
		}
		catch (ErrorCode::FileNotFoundException& e)
		{
			std::cout << e.what();
		}
	}

	return m_instance;
}
