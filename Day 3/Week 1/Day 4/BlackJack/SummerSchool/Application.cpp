#include "Application.h"
#include "MainMenu.h"
#include "PlayerNumberSelector.h"
#include <memory>


Application* Application::m_instance = nullptr;
Application::Application(const std::string& name, const int32_t x, const int32_t y)
	: m_window(sf::VideoMode(x, y), name)
{
	/* Point towards the application */
	if (m_instance == nullptr)
		m_instance = this;

	/* Switch our state to the Menu */
	stateController = std::make_unique<PlayerNumberSelector>();

	m_window.setVerticalSyncEnabled(true);
}

Application::~Application()
{
}

void Application::operator()()
{
	/* Run the game loop */
	gameLoop();

}

void Application::gameLoop()
{	
	/* Used for restricting the Update Loop to a fixed frame rate */
	sf::Clock updateClock;
	updateClock.restart();

	/* Update */
	while (m_window.isOpen())
	{
		/* Elapsed time between frames */
		sf::Time elapsedTime = updateClock.getElapsedTime();

		/* Fixed Update */
		if (elapsedTime > this->frameTime)
		{
			/* Reset our timer */
			updateClock.restart();

			/* Let the state do it's updating */
			stateController->update(m_window);

			/* Handling of events should be done as fast as the graphics card allows */
			handleEvent();

			/* Clear previous frame */
			m_window.clear(backgroundColor);

			/* Draw */
			stateController->draw(m_window);

			/* Display the new frame */
			m_window.display();

		}
	}
}

void Application::setSize(const int32_t x, const int32_t y)
{
	this->m_window.setSize(sf::Vector2u(x, y));
}

void Application::setTitle(const std::string& newTitle)
{
	this->m_window.setTitle(newTitle);
}

Application* Application::getInstance()
{
	return m_instance;
}

sf::Vector2u Application::getSize()
{
	return this->m_window.getSize();
}

void Application::handleEvent()
{
	while (m_window.pollEvent(this->m_event))
	{
		if (m_event.type == sf::Event::Closed)
			m_window.close();
	}
}
