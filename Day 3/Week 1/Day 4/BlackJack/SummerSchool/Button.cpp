#include "Button.h"

Button::Button()
{
	setUpButton();
}

Button::Button(const sf::Vector2f& position, const sf::Vector2f& size)
{
	m_hitbox.setSize(size);
	//m_hitbox.setPosition(sf::Vector2f(position.x - m_hitbox.getSize().x / 2, position.y - m_hitbox.getSize().y / 2));
	setPosition(position);
	m_text.setPosition(position);

	setUpButton();
}

void Button::setUpButton()
{
	setColour(sf::Color::Blue);
	setFont("impact.ttf");
	
	m_text.setFont(m_font);
	m_text.setString("Default String!");
	m_text.setFillColor(sf::Color::Black);
	m_text.setCharacterSize(24);
}

sf::Font Button::getFont() const
{
	return m_font;
}

void Button::setFont(const std::string& font)
{
	m_font.loadFromFile(font);
}

sf::Text Button::getText() const
{
	return m_text;
}

void Button::setText(const std::string& text)
{
	m_text.setString(text);
}

sf::Color Button::getColour() const
{
	return m_hitbox.getFillColor();
}

void Button::setColour(const sf::Color& color)
{
	m_hitbox.setFillColor(color);
}

sf::Vector2f Button::getPosition() const
{
	return m_hitbox.getPosition();
}

void Button::setPosition(const sf::Vector2f& position)
{
	m_hitbox.setPosition(sf::Vector2f(position.x - m_hitbox.getSize().x / 2, position.y - m_hitbox.getSize().y / 2));
	m_text.setPosition(sf::Vector2f(position.x - m_hitbox.getSize().x / 2, position.y - m_hitbox.getSize().y / 2));
}

sf::Vector2f Button::getSize() const
{
	return m_hitbox.getSize();
}

void Button::setSize(const sf::Vector2f& size)
{
	m_hitbox.setSize(size);
}

bool Button::isCollisonWithPoint(const sf::Vector2f& point) const
{
	sf::RectangleShape mouseRect;
	mouseRect.setPosition(point);

	if (isCollisionWithRect(mouseRect))
		return true;
	return false;
}

bool Button::isCollisionWithRect(const sf::RectangleShape& hitBox) const
{
	/* AABB Collision: true = there is a collision */

	if (this->getPosition().x < hitBox.getPosition().x + hitBox.getSize().x &&
		this->getPosition().x + m_hitbox.getSize().x > hitBox.getPosition().x &&
		this->getPosition().y < hitBox.getPosition().y + hitBox.getSize().y &&
		this->getPosition().y + m_hitbox.getSize().y > hitBox.getPosition().y)
	{
		return true;
	}
	return false;
}

void Button::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(m_hitbox);
	target.draw(m_text);
}
