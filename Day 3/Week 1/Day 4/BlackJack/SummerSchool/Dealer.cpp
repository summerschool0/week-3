
#include "Dealer.h"
#include "Card.h"
#include "Player.h"

#include <algorithm>
#include <ctime>
#include <cstdlib>
#include <utility>
#include <random>

Dealer::Dealer() : m_deck(), m_shoe()
{
}

Dealer::~Dealer()
{
}

void Dealer::Shuffle()
{
	size_t numCards = m_deck.GetNumberOfCards();
	Card** cards = m_deck.GetCards();

	std::random_device rd;
	std::mt19937 g(rd());

	std::shuffle(cards, cards + numCards, g);

	//std::random_shuffle(cards, cards + numCards, std::rand()); <- This doesn't work
	
	m_shoe.SetCards(numCards, cards);
}

void Dealer::Deal(Player * player)
{
	Card* card = m_shoe.NextCard();
	if (card == nullptr) return;

	player->GetHand().AddCard(card);
}

void Dealer::AddPlayer(Player* player)
{
	m_bettedMoney.insert(std::make_pair(player, 0.0));
}

std::unordered_map<Player*, double>& Dealer::getBettedMoney()
{
	return m_bettedMoney;
}

bool Dealer::Bet(Player* player, double amount)
{
	auto money = player->getTotalMoney() - amount;

	if (money < 0)
		return false;

	player->setTotalMoney(money);
	m_bettedMoney[player] = amount;

	return true;
}
