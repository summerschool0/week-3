#pragma once
#include <iostream>
#include "Card.h"


Card::Card(Suit suit, Rank rank) : m_suit(suit), m_rank(rank)
{
}

void Card::Print()
{
	printf("%c of ", m_rank);
	switch (m_suit)
	{
	case HEARTS:  printf("HEARTS"); break;
	case DIAMONDS:	 printf("DIAMONDS"); break;
	case CLUBS:  printf("CLUBS"); break;
	case SPADES:  printf("SPADES"); break;
	}
	printf("\n");
}

Card::~Card()
{
}
