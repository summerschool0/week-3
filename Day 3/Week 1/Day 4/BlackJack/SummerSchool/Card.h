#pragma once

enum Suit {
	HEARTS, 
	DIAMONDS,
	CLUBS, 
	SPADES
};

typedef char Rank;

class Card
{
public:
	Card(Suit suit, Rank rank);
	Card(const Card& other) = delete;
	Card& operator= (const Card& other) = delete;

	Rank GetRank() const { return m_rank; }
	Suit GetSuit() const { return m_suit; }
	
	void Print();

	~Card();

private:
	
	Suit m_suit;
	Rank m_rank;
};

