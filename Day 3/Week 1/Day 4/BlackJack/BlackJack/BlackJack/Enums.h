#pragma once
class Enums
{
public:
	enum class Rank
	{
		None = 0,

		Two = 2,
		Three,
		Four,
		Five,
		Six,
		Seven,
		Eight,
		Nine,
		Ten,
		Ace,
		Jack,
		Queen,
		King
	};

	enum class Suit
	{
		None = 0,

		Heart,
		Spade,
		Diamond,
		Clubs
	};

	enum class Action {
		HIT,
		STAND
	};
};

