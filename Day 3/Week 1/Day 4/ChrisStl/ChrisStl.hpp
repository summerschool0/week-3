#pragma once
#define durationCast(time) std::chrono::duration_cast<time>
#define durationCastMilliseconds std::chrono::duration_cast<std::chrono::milliseconds>

#include <vector>
#include <chrono>
#include <fstream>
#include <algorithm>
//#include "nameof.hpp"

namespace chris
{
	/* Overloads the << Operator to show the contents of a vector */
	template <typename T>
	std::ostream& operator << (std::ostream & os, const std::vector<T> & myVector)
	{
		os << "[";
		for (int i = 0; i < myVector.size(); ++i) {
			os << myVector[i];
			if (i != myVector.size() - 1)
				os << ", ";
		}
		os << "]" << std::endl;
		return os;
	}

	/* Overloads the << Operator to show the contents of a matrix */
	template <typename T>
	std::ostream& operator << (std::ostream & os, const std::vector<std::vector<T>> & myMatrix)
	{
		for (int i = 0; i < myMatrix.size(); ++i)
		{
			for (int j = 0; j < myMatrix[0].size(); ++j)
			{
				os << myMatrix[i][j] << " ";
			}
			os << std::endl;
		}
		return os;
	}

	/* Populates a vector with the contents of a .txt file*/
	template <typename T>
	void populateVectorFromFile(const std::string & fileName, std::vector<T> & myVector, int howMany)
	{
		std::ifstream file(fileName);
		T currentNumber;
		for (int i = 0; i < howMany - 1; ++i)
		{
			file >> currentNumber;
			myVector.push_back(currentNumber);
		}
		file.close();
	}

	/* Sorts a vector using Bubble Sort */
	template <typename T>
	void bubbleSort(std::vector<T> & myVector)
	{
		for (int i = 0; i < myVector.size(); i++)
		{
			for (int j = 0; j < myVector.size() - 1; j++)
			{
				if (myVector[j] > myVector[j + 1])
					std::swap(myVector[j], myVector[j + 1]);
			}
		}
	}

	/* Restarts a timer */
	void restartTimer(std::chrono::steady_clock::time_point & start)
	{
		start = std::chrono::high_resolution_clock::now();
	}

	/* Gets the current time */
	std::chrono::steady_clock::time_point getTimeNow()
	{
		return std::chrono::high_resolution_clock::now();
	}

	/* Gets the execution time */
	std::chrono::microseconds getExecutionTime(const std::chrono::steady_clock::time_point & startTime)
	{
		return std::chrono::duration_cast<std::chrono::microseconds>(getTimeNow() - startTime);
	}

	/* Prints the execution time */
	template <typename T>
	void printExecutionTime(T executionTime, const std::string & message, const std::string & secondsTypes = "microseconds")
	{
		std::cout << message << " " << executionTime.count() << " " << secondsTypes << std::endl;
	}
	
	//void runWithTime(){
		

	/* How to use execution time */
	// 	auto startTimer = getTimeNow();
	// printExecutionTime(getExecutionTime(startTimer), "A durat");
	/* Checks if an element is in bounds of a matrix */
	
	template <typename T>
	bool isInBounds(const std::vector<std::vector<T>> & myMatrix, int rowIndex, int colIndex)
	{
		if (rowIndex < 0 || colIndex < 0 || rowIndex > myMatrix.size() - 1 || colIndex > myMatrix[0].size() - 1)
			return false;
		return true;
	}

	/*                         Median Filter                         */

	/* Prints the neighbors (in a given range) of an element in a matrix */
	template <typename T>
	void printAllNeighborsInArange(const std::vector<std::vector<T>>& myMatrix, int range, int rowIndex, int colIndex)
	{
		for (int i = -range; i < range + 1; ++i)
		{
			for (int j = -range; j < range + 1; ++j)
			{
				if (isInBounds(myMatrix, rowIndex + i, colIndex + j))
				{
					std::cout << myMatrix[rowIndex + i][colIndex + j] << " ";
				}
			}
			std::cout << std::endl;
		}
	}

	/* Gets the neighbors (in a given range) of an element in a matrix */
	template <typename T>
	std::vector<T> getNeighborsInArange(const std::vector<std::vector<T>>& myMatrix, int range, int rowIndex, int colIndex)
	{
		std::vector<T> arrNeighbors;
		for (int i = -range; i < range + 1; ++i)
		{
			for (int j = -range; j < range + 1; ++j)
			{
				if (isInBounds(myMatrix, rowIndex + i, colIndex + j))
				{
					arrNeighbors.push_back(myMatrix[rowIndex + i][colIndex + j]);
				}
				//if (isInBounds(myMatrix, rowIndex + i, colIndex + j) == false)
					// do something
			}
			std::cout << std::endl;
		}
		return arrNeighbors;
	}
	
	/* Applies the median filter */
	template <typename T>
	void medianFilterToFile(std::ofstream & file, const std::vector<std::vector<T>> & myMatrix, int range, const std::string& output)
	{
		std::vector<int> neighbors;
		for (int i = 0; i < myMatrix.size(); ++i)
		{
			for (int j = 0; j < myMatrix[0].size(); ++j)
			{
				neighbors = getNeighborsInArange(myMatrix, range, i, j);
				bubbleSort(neighbors);
				//std::sort(neighbors.begin(), neighbors.end());
				file << neighbors[(neighbors.size() - 1) / 2] << " ";
			}
			file << "\n";
		}
	}

	/* Reads the input and calls the median filter method */ 
	void applyMedianFilter(const std::string & input, const std::string & output, int filter)
	{
		int range = filter / 2;
		int rows, cols;
		int maxPixel;
		
		std::ifstream inputFile(input);
		std::ofstream outputFile(output);

		std::string pgmFileType;
		inputFile >> pgmFileType;
		if (pgmFileType != "P2")
			return;
		outputFile << pgmFileType << "\n";

		inputFile >> rows;
		outputFile << rows << " ";
		inputFile >> cols;
		outputFile << cols << "\n";
		inputFile >> maxPixel;
		outputFile << maxPixel << "\n";

		std::vector<std::vector<int>> myMatrix(rows, std::vector<int>(cols));
		
		int currentNumber;
		for (int i = 0; i < myMatrix.size(); ++i)
		{
			for (int j = 0; j < myMatrix[0].size(); ++j)
			{
				inputFile >> currentNumber;
				myMatrix[i][j] = currentNumber;
			}
		}

		medianFilterToFile(outputFile,myMatrix,range,output);
		inputFile.close();
		outputFile.close();
	}

	/* Duplicates a vector's ends */
	template <typename T>
	void duplicateEndsOfVector(std::vector<T>& myVector)
	{
		myVector.insert(myVector.begin(), myVector[0]);
		myVector.push_back(myVector[myVector.size() - 1]);
	}

	/* Bords the vector with the margins */
	template <typename T>
	void MedianFilterVectorBording(std::vector<std::vector<T>> & myMatrix, int range)
	{
		for (int j = 0; j < myMatrix.size(); j++)
		{
			for (int i = 0; i < range; ++i)
				duplicateEndsOfVector(myMatrix[j]);
		}

		for (int i = 0; i < range; ++i)
		{
			myMatrix.insert(myMatrix.begin(), myMatrix[0]);
			myMatrix.push_back(myMatrix[myMatrix.size() - 1]);
		}
	}
}



