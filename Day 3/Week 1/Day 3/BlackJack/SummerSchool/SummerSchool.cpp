// SummerSchool.cpp : Defines the entry point for the console application.
//


#include "Player.h"
#include "Game.h"
#include "NaturalPolicy.h"
#include "WinnerTakesAll.h"

int main()
{
	Player* players[] =
	{
		new Player("Player 1"),
		new Player("Player 2")
	};
	size_t numPlayers = sizeof(players) / sizeof(Player*);

	NaturalPolicy* naturalPolicy = new NaturalPolicy();
	WinnerTakesAll* winnerTakesAll = new WinnerTakesAll();

	Game game(numPlayers, players, naturalPolicy);

	game.Play();

	for (Player* player : players)
	{
		delete player;
		player = nullptr;
	}
	
    return 0;
}

