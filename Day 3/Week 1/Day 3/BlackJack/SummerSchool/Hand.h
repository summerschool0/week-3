#pragma once

#include "CardsStack.h"

class Card;

class Hand : public CardStack
{
public:
	Hand();
	~Hand();

	void AddCard(Card* card);
	size_t GetValue() const;

	void Print();
};

