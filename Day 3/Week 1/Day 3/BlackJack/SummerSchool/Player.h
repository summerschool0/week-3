#pragma once
#include "Hand.h"
#include "IPlayer.h"

enum Action
{
	HIT,
	STAND
};

constexpr double defaultTotalMoney = 500;
class Player : IPlayer
{
public:

	Player(const char* name, double totalMoney = defaultTotalMoney);
	~Player();

	const char* GetName() const;
	Hand& GetHand();

	Action GetAction() const;
	double getTotalMoney() const;

	void setTotalMoney(double totalMoney);

private:
	char* m_name;
	Hand m_hand;

	double m_totalMoney;
};

