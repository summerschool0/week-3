
#include "Shoe.h"


Shoe::Shoe() :
	Deck(nullptr, 0),
	m_nextCardIndex(0)
{
	// Empty 
}


Shoe::~Shoe()
{
	delete[] m_cards;
	m_cards = nullptr;
}

void Shoe::SetCards(size_t numCards, Card** cards)
{
	delete[] m_cards;
	m_cards = nullptr;

	m_nextCardIndex = 0;
	m_numCards = numCards;
	m_cards = new Card * [numCards];
	for (size_t idx = 0; idx < numCards; ++idx)
	{
		m_cards[idx] = cards[idx];
	}
}

Card* Shoe::NextCard()
{
	if (m_nextCardIndex == m_numCards)
	{
		return nullptr;
	}

	return m_cards[m_nextCardIndex++];
}
