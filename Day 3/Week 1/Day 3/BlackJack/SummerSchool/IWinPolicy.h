#pragma once
class IWinPolicy
{
public:
	virtual double getPrize(int prizePool) = 0;
};