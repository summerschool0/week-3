#pragma once

#include "Dealer.h"
#include "IWinPolicy.h"

class Player;

class Game
{
public:
	Game(size_t numPlayers, Player** players, IWinPolicy* m_winPolicy);
	~Game();

	void Play();
	void PlayersBet();
	void SharePrize();

private:
	void DealInitialCards();
	void GameLoop();
	void PrintPlayers() const;

	size_t m_numPlayers;
	Player** m_players;
	Dealer m_dealer;

	IWinPolicy* m_winPolicy;
};

