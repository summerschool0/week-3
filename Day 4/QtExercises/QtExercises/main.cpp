#include <QtCore/QCoreApplication>

#include <iostream>
#include <QStringList>


#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);

	{
		QEventLoop loop;
		QNetworkAccessManager nam;
		QNetworkRequest req(QUrl("http://google.com"));
		QNetworkReply* reply = nam.get(req);
		QObject::connect(reply, &QNetworkReply::finished, &loop, &QEventLoop::quit);
		loop.exec();
		QByteArray buffer = reply->readAll();
		std::cout << buffer.constData() << std::endl;
	}
	QStringList args = a.arguments();
	for (auto arg : args)
		std::cout << arg.toStdString() << std::endl;
	return a.exec();
}
