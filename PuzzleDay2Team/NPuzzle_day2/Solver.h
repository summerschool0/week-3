#pragma once

#include "State.h"
#include "io.h"

#include <iostream>
#include <set>
#include <queue>
#include <algorithm>

template <class State_t>
class Heuristics
{
public:

	static size_t Difference(size_t first, size_t second)
	{
		return (first > second) ? first - second : second - first;
	}
	static size_t GetManhattenDistance(const State_t& state)
	{
		auto getDistance = [](size_t goal, size_t actual)
		{
			return Difference(goal / State_t::Dimension, actual / State_t::Dimension) + Difference(goal % State_t::Dimension, actual % State_t::Dimension);
		};

		size_t distance = 0u;
		size_t index = 0;
	
		for (auto it = state.GetData().begin(); it != state.GetData().end(); ++it, ++index)
			if (*it != 0)
				distance += getDistance(index, *it - 1);

		return distance;
	}

	static size_t GetNumberOfLiniarConflicts(const State_t& state)
	{
		auto hasLiniarConflicts = [](auto begin, auto end, auto check)
		{
			auto it = std::find_if(begin, end, check);
			if (it != end)
			{
				auto next = std::find_if(std::next(it), end, check);
				for (auto otherIt = next; next != end; otherIt = std::find_if(std::next(otherIt, end, check)))
				{
					if (*it > *next) return true;
				}
			}
		};

		size_t numLiniarConflicts = 0u;
		for (size_t row = 0u; row < State_t::Dimension; ++row)
		{
			if (hasLiniarConflicts(state.GetRowIteratorBegin(row), state.GetRowIteratorEnd(row),
				[row](State_t::ElementType tile) 
			{ return (tile != 0) && (tile - 1) / State_t::Dimension == row; }))
			{
				++numLiniarConflicts;
			}
		}

		for (size_t column = 0u; columns < State_t::Dimension; ++column)
		{
			if (hasLiniarConflicts(state.GetColumnIteratorBegin(column), state.GetColumnIteratorEnd(column)),
				[column](auto tile)
				{return (tile != 0) && (tile - 1) % State_t::Dimension == column;})
			{
				++numLiniarConflicts;
			}
		}
	}

	using Node = std::pair<State_t, Moves>;
	static size_t GetAStarDistance(const Node& node)
	{
		return GetManhattenDistance(node.first) + node.second.size();
	}
};


class Solver
{
public:
	template <class State_t>
	static size_t ComputeHash(const State_t& state)
	{
		static const std::hash<State_t::ElementType> hasher;

		size_t seed = 0u;
		for (auto&& value : state.GetData())
		{
			seed ^= hasher(value) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
		}

		return seed;
	};
	
#define GreedyAproach
#ifdef GreedyAproach
	template <class State_t>
	static Moves SolveBFS(const State_t& initialState)
	{
		std::cout << "Solving..." << std::endl << initialState;
		Validate(initialState);
		if (initialState.IsGoalState()) return {}; // no moves required. Initial state is goal state.

		using Node = std::pair<State_t, Moves>;

		auto priorityCompare = [](const Node& first, const Node& second) -> bool
		{
			return Heuristics<State_t>::GetAStarDistance(first) > Heuristics<State_t>::GetAStarDistance(second);
		};
		std::priority_queue<Node, std::vector<Node>, decltype(priorityCompare)> openSet(priorityCompare);
		openSet.push({ initialState,{} });

		// Create a comparator so std::set can work with the State instances.
		// It doesn't really make sense to compare states otherwise...
		auto stateEquals = [](const State_t& first, const State_t& second)
		{
			return first.GetData() == second.GetData();
		};

		std::unordered_set<
			State_t,
			std::function<size_t(const State_t&)>,
			decltype(stateEquals)
		> closedSet(8u, &Solver::ComputeHash<State_t>, stateEquals);

		while (!openSet.empty())
		{
			auto currentNode = openSet.top();
			auto&& currentState = currentNode.first;
			auto&& currentMoves = currentNode.second;
			openSet.pop();

			// some logging
			static size_t maxDepth = 0;
			if (currentMoves.size() > maxDepth)
			{
				maxDepth = currentMoves.size();
				std::cout << "Max Depth: " << maxDepth << std::endl;
			}
			// end logging

			if (currentState.IsGoalState())
			{
				std::cout << "Visited: " << closedSet.size() << std::endl;
				return currentMoves;
			}

			closedSet.insert(currentState);

			for (auto&& childMovePair : currentState.GetChildren())
			{
				auto&& childState = childMovePair.first;
				MoveDirection move = childMovePair.second;

				if (closedSet.find(childState) == closedSet.end())
				{
					Moves childMoves = currentMoves;
					childMoves.push_back(move);
					openSet.push({ std::move(childState), std::move(childMoves) });
				}
			}
		}

		throw std::runtime_error("Couldn't solve");
	}
#endif // GreedyAproach

private:
	template <class State_t>
	static void Validate(const State_t& state)
	{
		if (!state.IsValid())
		{
			throw std::runtime_error("Initial state is invalid!");
		}

		if (!state.IsSolvable())
		{
			throw std::runtime_error("Initial state is not solvable!");
		}
	}
};