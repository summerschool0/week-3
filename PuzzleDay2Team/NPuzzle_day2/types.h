#pragma once
#include <vector>

enum class MoveDirection : uint8_t
{
	LEFT,
	RIGHT,
	UP,
	DOWN,
};
using Moves = std::vector<MoveDirection>;
