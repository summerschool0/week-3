#pragma once
#include <memory>
#include <array>
#include "State.h"

class Node
{
private:
	const int matrixSize = 9;
	const int rowSize = 3;

	enum class Direction
	{
		Up,
		Down,
		Left,
		Right,

		None
	};

	static constexpr int emptySpace = 0;

public:
	bool equals(const Node& other);
	Node(const State& config, std::shared_ptr<Node> parent = nullptr);
	~Node() = default;
	Node(const Node& other);

	bool operator == (const Node& second);
	friend std::ostream& operator<<(std::ostream& os, Node& node);

	std::shared_ptr<Node> GetSuccesor();

	State nodeState;
	std::shared_ptr<Node> Parent;
private:
	Direction currentDirection;
};

