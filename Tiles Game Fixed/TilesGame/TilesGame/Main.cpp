#include <iostream>
#include "Game.h"
#include <array>
#include <list>
#include "State.h"
#include "Node.h"
#include <unordered_set>
#include <queue>
#include <memory>
//#define durationCast(time) std::chrono::duration_cast<time>
//#define durationCastMilliseconds std::chrono::duration_cast<std::chrono::milliseconds>
//
//
///* Restarts a timer */
//void restartTimer(std::chrono::steady_clock::time_point& start)
//{
//	start = std::chrono::high_resolution_clock::now();
//}
//
///* Gets the current time */
//std::chrono::steady_clock::time_point getTimeNow()
//{
//	return std::chrono::high_resolution_clock::now();
//}
//
///* Gets the execution time */
//std::chrono::milliseconds getExecutionTime(const std::chrono::steady_clock::time_point& startTime)
//{
//	return std::chrono::duration_cast<std::chrono::milliseconds>(getTimeNow() - startTime);
//}
//
///* Prints the execution time */
//template <typename T>
//void printExecutionTime(T executionTime, const std::string& message, const std::string& secondsTypes = "milliseconds")
//{
//	std::cout << message << " " << executionTime.count() << " " << secondsTypes << std::endl;
//}


class NodeHasher
{
public:
	int operator()(std::shared_ptr<Node> node) const
	{
		auto config = node->nodeState.getConfig();
		int result = 0;
		for (int i = 0; i < config.size(); ++i)
		{
			result *= 10;
			result += config[i];
		}

		return result;
	}
};

class NodeEquals
{
public:
	bool operator()(std::shared_ptr<Node> first, std::shared_ptr<Node> second) const
	{
		return *first == *second;
	}
};


std::shared_ptr<Node> BFS(std::shared_ptr<Node> start, const std::shared_ptr<Node> goal)
{
	/* Starting position */
	std::queue<std::shared_ptr<Node>> fringe;
	fringe.push(start);

	/* List of visited nodes */
	std::unordered_set<std::shared_ptr<Node>, NodeHasher, NodeEquals> closed;

	/* BFS Traversal */
	bool found = false;
	while (!found && !fringe.empty())
	{

		std::shared_ptr<Node> currentNode = fringe.front();
		fringe.pop();

		/* Populate the fringe */
		// TODO: Use set custom hash
		auto it = closed.find(currentNode);
		if (it == closed.end())
		{
			while (auto succesor = currentNode->GetSuccesor())
			{
				auto succesorIt = closed.find(succesor);
				if (succesorIt == closed.end())
					fringe.push(succesor);
			}

			closed.insert(currentNode);
		}

		/* Did we find the goal? */
		if (!(currentNode->equals(*goal)))
			continue;

		found = true;
		return currentNode;
	}

	return nullptr;
}

int main()
{
	Game game;

	std::cout << game.getNumberOfInversion();
	std::cout << game.isSolvable();
	std::array<int, 9> a = { 1, 2, 3, 4, 5, 6, 7, 8, 0 };
//	auto startTimer = getTimeNow();
	auto result = BFS(std::make_shared<Node>(game.myState), std::make_shared<Node>(State(a)));
// printExecutionTime(getExecutionTime(startTimer), "A durat");
	while (result->Parent)
	{
		std::cout << *result << '\n';
		result = result->Parent;
	}

	std::cin.get();
	return 0;
}