#include "Game.h"

Game::Game() : Game("fis.in")
{
}

Game::Game(const std::string& fileName)
{
	readFromFile(fileName);
}

bool Game::isSolvable()
{
	int invCount = getNumberOfInversion();
	/* If the grid width is odd, then the number of inversions in a solvable situation is even. */
	return invCount % 2 == 0 ? true : false;
}

int Game::getNumberOfInversion() const
{
	int invCount = 0;

	auto beginIt = myState.getConfig().cbegin();
	auto lastPosIt = --myState.getConfig().cend();
	auto endIt = myState.getConfig().cend();

	for (auto i = beginIt; i != lastPosIt; ++i)
	{
		for (auto j = beginIt + 1; j != endIt; ++j)
		{
			if (*i > * j)
				invCount++;
		}
	}
	return invCount;
}

void Game::readFromFile(const std::string& fileName)
{
	// TODO
	std::ifstream myFile(fileName);
	for (int i = 0, fileInt = 0; i < myState.getConfig().size(); i++, fileInt = i)
	{
		myFile >> fileInt;
		myState.getConfig()[i] = fileInt;
	}
	myFile.close();
}
