#pragma once
#ifndef Utils_H
#define Utils_H
// Header code
#include <vector>
#include <chrono>
#include <fstream>
#include <algorithm>
#include <iostream>
#include <unordered_set>


template <typename T>
std::ostream& operator<<(std::ostream& os, const std::vector<T>& v)
{
	os << "\n";
	for (int i = 0; i < v.size(); ++i) {
		os << v[i];
		if (i % 3 == 0)
			os << '\n';
	}
	os << "]\n";
	return os;
}


template <typename T>
std::ostream& operator<<(std::ostream& os, const std::array<T,9>& v)
{
	int counter = 0;
	for (int i = 0; i < v.size(); ++i) {
		os << v[i];
		counter++;
		if (counter == 3)
		{
			os << '\n';
			counter = 0;
		}
		else
		{
			os << ' ';
		}
	}
	return os;
}

template <typename T>
std::ostream& operator << (std::ostream& os, const std::unordered_set<T>& set)
{
	os << "S:[";
	for (const auto& element : set)
	{
		os << element << ", ";
	}
	os << "]\n";
	return os;
}
#endif 