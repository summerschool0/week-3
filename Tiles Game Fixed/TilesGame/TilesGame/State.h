#pragma once
#include <array>
#include "Utils.h"

class State
{
public:
	State(const std::array<int, 9>& config);
	State() = default;

private:
	std::array<int, 9> config;
	int cursor = -1;

public:

	int getCursor();
	std::array<int, 9>& getConfig();
	const std::array<int, 9> & getConfig() const;
	bool operator == (const State& other);

	friend std::ostream& operator<<(std::ostream& os, State& state);
};

