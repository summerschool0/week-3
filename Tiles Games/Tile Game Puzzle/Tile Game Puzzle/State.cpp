#include "State.h"

State::State(const std::array<int, 9>& config)
	: config(config), cursor(findCursor())
{}

int State::findCursor()
{
	for (int i = 0; i < config.size(); ++i)
		if (config[i] == 0)
			return cursor;

	throw "ERROR: Invalid state recieved";
}

int State::getCursor()
{
	return this->cursor;
}

std::array<int, 9> & State::getConfig()
{
	return config;
}

const std::array<int, 9> & State::getConfig() const
{
	return config;
}

bool State::operator==(const State& other)
{
	return this->config == other.config;
}
