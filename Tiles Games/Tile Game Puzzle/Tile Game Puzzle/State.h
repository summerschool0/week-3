#pragma once
#include <array>

class State
{
public:
	State(const std::array<int, 9>& config);
	State() = default;
private:
	std::array<int, 9> config;
	int cursor;
	int findCursor();

public:

	int getCursor();
	std::array<int, 9>& getConfig();
	const std::array<int, 9> & getConfig() const;
	bool operator == (const State& other);
};

