#include "Node.h"

Node::Node(const State& state) :
	state(state)
{
}

Node::Node(const std::array<int, 9> & config) :
	state(config)
{}

Node* Node::GetSuccesor()
{
	State newState(this->state);
	auto config = newState.getConfig();
	auto cursor = newState.getCursor();

	int row = (cursor % 3) - 1;
	int column = (cursor / 3) - 1;

	switch (currentDirection)
	{
	case Direction::Up:

		currentDirection = static_cast<Direction>(static_cast<int>(currentDirection) + 1);
		if (row > 0)
		{
			config[cursor] = config[cursor - 3];
			return new Node(newState);
		}
	case Direction::Down:
		currentDirection = static_cast<Direction>(static_cast<int>(currentDirection) + 1);
		if (row < 2)
		{
			config[cursor] = config[cursor + 3];
			return new Node(newState);
		}

	case Direction::Left:
		currentDirection = static_cast<Direction>(static_cast<int>(currentDirection) + 1);
		if (column > 0)
		{
			config[cursor] = config[cursor - 1];
			return new Node(newState);
		}
	case Direction::Right:
		currentDirection = static_cast<Direction>(static_cast<int>(currentDirection) + 1);
		if (column < 2)
		{
			config[cursor] = config[cursor + 1];
			return new Node(newState);
		}
	default:
		return nullptr;
	}
}

State Node::getState() const
{
	return this->state;
}
