#pragma once
#include <memory>
#include <array>

#include "State.h"

class NodeComparator
{
	size_t operator()(const Node& node)
	{
		auto config = node.getState().getConfig();
		int result = 0;
		for (int i = 0; i < config.size(); ++i)
		{
			result *= 10;
			result += config[i];
		}

		return result;
	}
};

class Node
{
private:
	const int matrixSize = 9;
	const int rowSize = 3;

	enum class Direction
	{
		Up,
		Down,
		Left,
		Right,

		None
	};

	static constexpr int emptySpace = 0;

public:
	Node(const State& state);
	Node(const std::array<int, 9> & config);
	~Node() = default;

	Node* GetSuccesor();
	State getState() const;

private:
	State state;
	Node* Parent;
	Direction currentDirection;
};